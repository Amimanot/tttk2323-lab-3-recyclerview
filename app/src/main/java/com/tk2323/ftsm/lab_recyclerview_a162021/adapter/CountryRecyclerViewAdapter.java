package com.tk2323.ftsm.lab_recyclerview_a162021.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tk2323.ftsm.lab_recyclerview_a162021.CountryObject;
import com.tk2323.ftsm.lab_recyclerview_a162021.DetailActivity;
import com.tk2323.ftsm.lab_recyclerview_a162021.R;

import java.util.List;

/**
 * Created by Amir Aiman Othman on 3/27/2018.
 */

public class CountryRecyclerViewAdapter extends RecyclerView.Adapter<CountryRecyclerViewAdapter.CountryRecyclerViewHolder>{

    public List<CountryObject> countryList;
    private Context context;

    public CountryRecyclerViewAdapter(Context context, List<CountryObject> countryList)
    {
        this.context = context;
        this.countryList = countryList;
    }

    @Override
    public CountryRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View rowItemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, null);
        CountryRecyclerViewHolder countryRecyclerViewHolder = new CountryRecyclerViewHolder(rowItemLayout);

        return countryRecyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(CountryRecyclerViewHolder holder, int position) {
        holder.countryName.setText(countryList.get(position).getName());
        holder.countryImage.setImageResource(countryList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return this.countryList.size();
    }

    public class CountryRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView countryName;
        public ImageView countryImage;

        public CountryRecyclerViewHolder(View itemView) {
            super(itemView);
            countryName = itemView.findViewById(R.id.tv_country);
            countryImage = itemView.findViewById(R.id.img_country);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Toast.makeText(view.getContext(), "Country Name : " + countryList.get(getAdapterPosition()).getName(), Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(view.getContext(), DetailActivity.class);
            intent.putExtra("Name", countryList.get(getAdapterPosition()).getName());
            view.getContext().startActivity(intent);
        }
    }
}
