package com.tk2323.ftsm.lab_recyclerview_a162021;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tk2323.ftsm.lab_recyclerview_a162021.adapter.CountryRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    LinearLayoutManager linearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<CountryObject> allCountryInfo = getAllCountryInfo();

        RecyclerView recyclerView = findViewById(R.id.rv_country);
        linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);

        CountryRecyclerViewAdapter countryRecyclerViewAdapter = new CountryRecyclerViewAdapter(MainActivity.this, allCountryInfo);
        recyclerView.setAdapter(countryRecyclerViewAdapter);
    }

    private List<CountryObject> getAllCountryInfo()
    {
        List<CountryObject> allCountry = new ArrayList<CountryObject>();

        allCountry.add(new CountryObject("United States", R.drawable.img_newyork));
        allCountry.add(new CountryObject("Canada", R.drawable.img_canada));
        allCountry.add(new CountryObject("United Kingdom", R.drawable.img_uk));
        allCountry.add(new CountryObject("Germany", R.drawable.img_germany));
        allCountry.add(new CountryObject("Sweden", R.drawable.img_sweden));

        return allCountry;
    }

}
