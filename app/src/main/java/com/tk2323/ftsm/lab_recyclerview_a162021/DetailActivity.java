package com.tk2323.ftsm.lab_recyclerview_a162021;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {
    TextView txtName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        txtName = findViewById(R.id.tv_name);
        String country = getIntent().getStringExtra("Name");

        txtName.setText(country);
    }
}
