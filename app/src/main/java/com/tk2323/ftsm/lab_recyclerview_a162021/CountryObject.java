package com.tk2323.ftsm.lab_recyclerview_a162021;

/**
 * Created by Amir Aiman Othman on 3/27/2018.
 */

public class CountryObject {
    String name;
    int image;

    public CountryObject(String name, int image) {
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
